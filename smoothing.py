import numpy as np
from typing import List, Union, Dict

from astar import Node


class Point:
    def __init__(self, fixed: bool, node: Node = None, x: int = None, y: int = None):
        if node is None and (x is None or y is None):
            raise ValueError("node or x and y should present")
        self.x = node.x if node is not None else x
        self.y = node.y if node is not None else y
        self.fixed = fixed

    def node(self) -> Node:
        return Node(self.x, self.y)


def neighbors(i: int, pts: Union[List[Point], Dict[int, Point]]) -> List[Point]:
    ns = []
    if i - 1 >= 0:
        ns.append(pts[i - 1])
    if i + 1 < len(pts):
        ns.append(pts[i + 1])
    return ns


def laplacian_op(points: List[Point]) -> Point:
    x = np.mean([point.x for point in points])
    y = np.mean([point.y for point in points])
    return Point(False, x=round(x), y=round(y))


def laplacian_smooth(original: List[Point], n: int = 50) -> List[Point]:
    v_var = [i if not original[i].fixed else -1 for i in range(len(original))]
    v_var = filter(lambda i: i >= 0, v_var)

    points = original
    for _ in range(n):
        q = points
        for i in v_var:
            ns = neighbors(i, q)
            if len(ns) != 0:
                points[i] = laplacian_op(ns)
    return points


def calculate_b(p: Point, o: Point, q: Point, alpha: float) -> Point:
    x = p.x - (alpha * o.x + (1 - alpha) * q.x)
    y = p.y - (alpha * o.y + (1 - alpha) * q.y)
    return Point(False, x=round(x), y=round(y))


def calculate_p(pi: Point, bi: Point, bj: List[Point], beta: float) -> Point:
    bjx = np.mean([point.x for point in bj])
    bjy = np.mean([point.y for point in bj])

    x = pi.x - (beta * bi.x + (1 - beta) * bjx)
    y = pi.y - (beta * bi.y + (1 - beta) * bjy)
    return Point(False, x=round(x), y=round(y))


def hc_smooth(original: List[Point], n: int = 50,
              alpha: float = 0.2, beta: float = 0.2) -> List[Point]:
    v_var = [i if not original[i].fixed else -1 for i in range(len(original))]
    v_var = filter(lambda i: i >= 0, v_var)

    b = {}

    if n % 10 == 0:
        print("10th iteration of smoothing")

    points = original
    for _ in range(n):
        q = points

        for i in v_var:
            ns = neighbors(i, q)
            if len(ns) != 0:
                points[i] = laplacian_op(ns)
            b[i] = calculate_b(points[i], original[i], q[i], alpha)

        for i in v_var:
            ns = neighbors(i, b)
            if len(ns) != 0:
                points[i] = calculate_p(points[i], b[i], ns, beta)

    return points


def laplacian_path(not_sm_path: List[Node], fix_step: int = 10,
                   drop: int = 4, n: int = 100) -> List[Node]:
    points = [Point(i % fix_step == 0, not_sm_path[i]) for i in range(0, len(not_sm_path), drop)]
    path = laplacian_smooth(points, n)
    return [point.node() for point in path]


def hc_path(not_sm_path: List[Node], fix_step: int = 10, drop: int = 4,
            n: int = 100, alpha: float = 0.25, beta: float = 0.25) -> List[Node]:
    points = [Point(i % fix_step == 0, not_sm_path[i]) for i in range(0, len(not_sm_path), drop)]
    path = hc_smooth(points, n, alpha=alpha, beta=beta)
    return [point.node() for point in path]
