from functools import reduce

import numpy as np
from PIL import Image
from typing import List, Iterable

from ptypes import CostFieldType, RGBImageType, RGBImagesType


class ProcessingRule:
    def __init__(self, start_inclusive: int, end_inclusive: int, value: float):
        self.start_inclusive = start_inclusive
        self.end_inclusive = end_inclusive
        self.value = value


class ProcessingRules:
    def __init__(self, rules: List[ProcessingRule], normalize: bool):
        self.normalize = normalize
        self.rules = rules

    def __iter__(self) -> Iterable[ProcessingRule]:
        return iter(self.rules)


def combine_cost_fields(cost_fields: 'np.ndarray[CostFieldType]',
                        cost_weights: List[float]) -> CostFieldType:
    shape = cost_fields.shape
    total_cost_field = np.zeros((shape[1], shape[2]))

    for i in range(shape[0]):
        total_cost_field += cost_fields[i] * cost_weights[i]

    return total_cost_field


def min_max_normalize(features: CostFieldType, k: int = 10) -> CostFieldType:
    features = features.astype(np.double)
    features_min = features.min()
    return k * (features - features_min) / (features.max() - features_min)


def open_image(filename: str) -> RGBImageType:
    img = Image.open(filename)
    img.load()
    return np.asarray(img)


def open_images(filenames: List[str]) -> RGBImagesType:
    return [open_image(filename) for filename in filenames]


def create_cost_field(rgb_matrix: RGBImageType, rules: ProcessingRules) -> CostFieldType:
    r = rgb_matrix[:, :, 0]
    g = rgb_matrix[:, :, 1]
    b = rgb_matrix[:, :, 2]
    rgb_matrix = np.asarray([r, g, b])

    combined_matrix = reduce(lambda _1, _2: np.bitwise_or(_1, _2), rgb_matrix)
    cost_field = combined_matrix
    for rule in rules:
        cost_field[(rule.start_inclusive <= cost_field) & (cost_field <= rule.end_inclusive)] \
            = rule.value
    return min_max_normalize(cost_field) if rules.normalize else cost_field


def create_cost_fields(matrices: RGBImagesType,
                       rules: List[ProcessingRules]) -> 'np.ndarray[CostFieldType]':
    return np.asarray([create_cost_field(matrices[i], rules[i]) for i in range(len(rules))])
