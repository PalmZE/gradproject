from typing import List, Callable, Dict

from priorityq import PriorityQueue
from ptypes import CostFieldType


class Node:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.h = None

    def __str__(self) -> str:
        return "Node x={} y={}".format(self.x, self.y)

    def __eq__(self, other: 'Node') -> bool:
        return self.x == other.x and self.y == other.y

    def __hash__(self) -> int:
        return hash(self.x) * 31 + hash(self.y)

    def __lt__(self, other: 'Node') -> bool:
        return self.h < other.h


class Graph:
    def __init__(self, len_y: int, len_x: int):
        self.len_y = len_y
        self.len_x = len_x
        self.nodes = {}

    def neighbors(self, node: Node) -> List[Node]:
        node_coord = (node.x, node.y)
        if node_coord not in self.nodes:
            self.nodes[node_coord] = node
        neighbors = []

        x_range = range(node.x - 1, node.x + 2)
        y_range = range(node.y - 1, node.y + 2)

        for y in y_range:
            for x in x_range:

                if x == node.x and y == node.y:
                    continue

                if 0 <= x < self.len_x and 0 <= y < self.len_y:
                    coord = (x, y)
                    if coord in self.nodes:
                        neighbors.append(self.nodes[coord])
                    else:
                        neighbor = Node(x, y)
                        self.nodes[coord] = neighbor
                        neighbors.append(neighbor)
        return neighbors

    def reset(self):
        for (_, node) in self.nodes.items():
            node.h = None

    def __str__(self) -> str:
        return "Graph {} by {}".format(self.len_y, self.len_x)


def diagonal_h(start: Node, goal: Node, d: float = 1, d2: float = 2 ** 0.5) -> float:
    """
    Computes heuristic cost using diagonal distance
    :param start: starting Node
    :param goal: goal Node
    :param d: cost of non diagonal move
    :param d2: cost of diagonal move
    :return: total cost
    """
    dx = abs(start.x - goal.x)
    dy = abs(start.y - goal.y)
    return d * (dx + dy) + (d2 - 2 * d) * min(dx, dy)


def get_h(k: float = 1) -> Callable[[Node, Node, int, float], float]:
    return lambda start, goal, d=1, d2=2 ** 0.5: k * diagonal_h(start, goal, d, d2)


def get_g(cost_field: CostFieldType) -> Callable[[Node, Node], float]:
    """
    Creates g cost function given cost field
    """

    def g(start: Node, goal: Node) -> float:
        return (abs(start.x - goal.x) + abs(start.y - goal.y)) ** 0.5 * cost_field[goal.y, goal.x]

    return g


def reconstruct_path(came_from: Dict[Node, Node], current: Node) -> List[Node]:
    total_path = []
    while current in came_from:
        total_path.append(current)
        current = came_from[current]
    return total_path


def a_star(start: Node, goal: Node,
           h: Callable[[Node, Node], float], g: Callable[[Node, Node], float],
           graph: Graph) -> List[Node]:
    visited_nodes = set([])
    nodes_queue = PriorityQueue()
    nodes_queue.push(start, 0)
    came_from = {start: None}
    g_costs = {start: 0}

    while not nodes_queue.is_empty():
        current = nodes_queue.pop()

        if current in visited_nodes:
            continue

        if current == goal:
            return reconstruct_path(came_from, current)

        visited_nodes.add(current)

        for neighbor in graph.neighbors(current):
            if neighbor in visited_nodes:
                continue

            g_cost = g_costs[current] + g(current, neighbor)

            if neighbor not in g_costs or g_cost < g_costs[neighbor]:
                g_costs[neighbor] = g_cost
                if neighbor.h is None:
                    neighbor.h = h(neighbor, goal)
                f = g_cost + neighbor.h
                nodes_queue.push(neighbor, f)
                came_from[neighbor] = current
