from collections import Callable

import numpy as np
from scipy import stats
from typing import List, Tuple

from astar import Node
from ptypes import CostFieldType
from session import Session

np.random.seed(0)


def learn(sess: Session,
          w_size: int,
          n_iter: int = 10,
          pop_size: int = 10,
          sigma: float = 0.1,
          alpha: float = 0.01,
          w_initial: 'np.ndarray[float]' = None) -> Tuple[CostFieldType, List[List[Node]], List[Node]]:
    w, paths = nes(sess, w_size, get_f(get_cost_function(sess)),
                   n_iter, pop_size, sigma, alpha, w_initial)
    field, path = sess.find_path(list(w[:-1]), w[-1:][0])
    return field, paths, path


def get_cost_function(sess: Session) -> 'Callable[[np.ndarray[float]] -> float]':
    return lambda w: cost_function(w, sess)


def cost_function(weights: 'np.ndarray[float]',
                  sess: Session) -> float:
    _, path = sess.find_path(list(weights[:-1]), weights[-1:][0])
    return len(path) * 100  # todo


def get_f(cost_f: 'Callable[[np.ndarray[float]], float]') -> 'Callable[[np.ndarray[float]], float]':
    return lambda w: -cost_f(w)


def nes(sess: Session,
        w_size: int,
        f: 'Callable[[np.ndarray[float]], float]',
        n_iter: int = 10,
        pop_size: int = 10,
        sigma: float = 0.1,
        alpha: float = 0.01,
        w_initial: 'np.ndarray[float]' = None) -> 'Tuple[np.ndarray[float], List[List[Node]]]':
    paths = []
    # our initial guess is random
    w = np.random.randn(w_size) if w_initial is None else w_initial

    for i in range(n_iter):
        _, path = sess.find_path(list(w[:-1]), w[-1:][0])
        paths.append(path)
        # todo plotting
        # print current fitness of the most likely parameter setting
        # if i % 20 == 0:
        #     print('iter %d. w: %s, solution: %s, reward: %f' %
        #           (i, str(w), str(solution), f(w)))
        # todo plotting

        weights_pop = np.random.randn(pop_size, w_size)  # samples from a normal distribution weights_pop(0,1)
        rewards = np.zeros(pop_size)
        for j in range(pop_size):
            w_try = w + sigma * weights_pop[j]  # jitter w using gaussian of sigma 0.1
            rewards[j] = f(w_try)  # evaluate the jittered version

        z_values = stats.zscore(rewards)
        w = w + alpha / (pop_size * sigma) * np.dot(weights_pop.T, z_values)
    return w, paths
