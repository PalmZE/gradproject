import matplotlib.patheffects as pe
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib.pyplot import cm
from typing import List

from astar import Node
from ptypes import RGBImagesType

sns.set_style("whitegrid",
              {'axes.grid': False})
_, ax = plt.subplots(1, 1)
ax.set_yticks([])
ax.set_xticks([])


def plot_map(rgb_images: RGBImagesType):
    for image in rgb_images:
        ax.imshow(image, alpha=0.4)


def plot_path(path: List[Node], color, label: str):
    points = np.asarray([[node.x, node.y] for node in path])
    ax.plot(points[:, 0], points[:, 1],
            c=color,
            path_effects=[pe.Stroke(linewidth=3, foreground='#000000'), pe.Normal()],
            label=label)


def plot_paths(paths: List[List[Node]], labels: List[str]):
    color = iter(cm.rainbow(np.linspace(0, 1, len(paths))))
    for path, label in zip(paths, labels):
        plot_path(path, next(color), label)


def show():
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.show()
