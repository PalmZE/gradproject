# coding=utf-8
"""
Параметры для модели:
Slope
Water
Cities
Roads
Vegetation

Экономическая стоимость земли
Экономическая стоимость трубы
Минимально допустимое расстояние до жилых домов и других объектов
Ecologically dangerous areas
Existing pipeline assets
_________________________________________________________________________________
1 Составление матриц весов для каждого признака
2 Обработка данных
todo
3 Сглаживание линий с радиусом R

4 Подбор оптимальных весв

5 Подсчет реальной стоимости

"""

import os

import astar as star
import plotting
import processing as pr
from session import Session
from smoothing import laplacian_path, hc_path

if __name__ == '__main__':
    filenames = ["mockdata/{}".format(f) for f in os.listdir("mockdata")]
    filenames = filter(lambda name: "png" in name, filenames)
    filenames = sorted(filenames)

    images = pr.open_images(filenames)
    plotting.plot_map(images)

    rules = [
        # cities rules
        pr.ProcessingRules([
            pr.ProcessingRule(1, 2 ** 32, 100000)
        ], normalize=False),
        # geology rules
        pr.ProcessingRules([
            pr.ProcessingRule(0, 60, 1),
            pr.ProcessingRule(80, 120, 2),
            pr.ProcessingRule(130, 170, 3)
        ], normalize=True),
        # roads rules
        pr.ProcessingRules([
            pr.ProcessingRule(1, 2 ** 32, 1)
        ], normalize=False),
        # slope rules
        pr.ProcessingRules([
            pr.ProcessingRule(0, 35, 1),
            pr.ProcessingRule(36, 60, 2),
            pr.ProcessingRule(61, 85, 3),
            pr.ProcessingRule(86, 110, 4),
            pr.ProcessingRule(111, 135, 5),
            pr.ProcessingRule(136, 2 ** 32, 6),
        ], normalize=True),
        # water rules
        pr.ProcessingRules([
            pr.ProcessingRule(1, 2 ** 32, 1)
        ], normalize=False)
    ]

    sess = Session(start=star.Node(249, 10),
                   goal=star.Node(379, 574),
                   rgb_images=images,
                   rules=rules)

    # cities, geology, roads, slope, water
    _, not_smooth1 = sess.find_path([100, 2, 100, 2, 100], 2)
    smooth1 = hc_path(not_smooth1, drop=4, n=1000, fix_step=5, alpha=0.9, beta=0.5)
    smooth1 = hc_path(not_smooth1, drop=8, n=1000, fix_step=5, alpha=0.9, beta=0.5)

    plotting.plot_paths([smooth1],
                        ['1'])

    plotting.show()
