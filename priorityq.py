import heapq


class PriorityQueue:
    def __init__(self):
        self.elements = []

    def is_empty(self) -> bool:
        return len(self.elements) == 0

    def push(self, item, priority: float):
        heapq.heappush(self.elements, (priority, item))

    def pop(self):
        return heapq.heappop(self.elements)[1]
