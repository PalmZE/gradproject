# coding=utf-8
"""
Параметры для модели:
Slope
Water
Cities
Roads
Vegetation

Экономическая стоимость земли
Экономическая стоимость трубы
Минимально допустимое расстояние до жилых домов и других объектов
Ecologically dangerous areas
Existing pipeline assets
_________________________________________________________________________________
1 Составление матриц весов для каждого признака
2 Обработка данных
todo
3 Сглаживание линий с радиусом R

4 Подбор оптимальных весв

5 Подсчет реальной стоимости

"""

import os

import numpy as np

import astar as star
import plotting
import processing as pr
from learning import learn
from session import Session
from smoothing import laplacian_path, hc_path

if __name__ == '__main__':
    filenames = ["mockdata2/{}".format(f) for f in os.listdir()]
    filenames = filter(lambda name: "png" in name, os.listdir())
    filenames = sorted(filenames)

    images = pr.open_images(filenames)
    plotting.plot_map(images)

    rules = [
        # cities rules
        pr.ProcessingRules([
            pr.ProcessingRule(1, 2 ** 32, 100000)
        ], normalize=False),
        # geology rules
        pr.ProcessingRules([
            pr.ProcessingRule(0, 60, 1),
            pr.ProcessingRule(80, 120, 2),
            pr.ProcessingRule(130, 170, 3)
        ], normalize=True),
        # near roads
        pr.ProcessingRules([
            pr.ProcessingRule(150, 2**32, 0),
            pr.ProcessingRule(40, 60, 1),
            pr.ProcessingRule(80, 120, 2),
            pr.ProcessingRule(0, 10, 10)
        ], normalize=True),
        # roads rules
        pr.ProcessingRules([
            pr.ProcessingRule(1, 2 ** 32, 1)
        ], normalize=False),
        # slope rules
        pr.ProcessingRules([
            pr.ProcessingRule(0, 35, 1),
            pr.ProcessingRule(36, 60, 2),
            pr.ProcessingRule(61, 85, 3),
            pr.ProcessingRule(86, 110, 4),
            pr.ProcessingRule(111, 135, 5),
            pr.ProcessingRule(136, 2 ** 32, 6),
        ], normalize=True),
        # vegetation rules
        pr.ProcessingRules([
            pr.ProcessingRule(0, 10, 0),
            pr.ProcessingRule(30, 60, 1),
            pr.ProcessingRule(80, 120, 2),
            pr.ProcessingRule(130, 150, 3),
        ], normalize=True),
        # water rules
        pr.ProcessingRules([
            pr.ProcessingRule(1, 2 ** 32, 1)
        ], normalize=False)
    ]

    sess = Session(start=star.Node(130, 70),
                   goal=star.Node(615, 490),
                   rgb_images=images,
                   rules=rules)

    _, learn_paths, not_smoothed_path = learn(sess, 8, n_iter=2, pop_size=5,
                                              w_initial=np.asarray([100,  # cities
                                                                    1,  # geology
                                                                    1,  # near_roads
                                                                    5,  # roads
                                                                    1,  # slope
                                                                    0.5,  # vegetation
                                                                    5,  # water
                                                                    5  # k
                                                                    ]))

    smoothed_path = hc_path(not_smoothed_path, drop=4, n=100, fix_step=3, alpha=0.9, beta=0.3)

    all_paths = learn_paths
    labels = ['Learn path #{}'.format(i) for i in range(len(learn_paths))]

    all_paths.append(smoothed_path)
    labels.append('Final pipeline')

    plotting.plot_paths(all_paths, labels)

    plotting.show()
