from time import time

from typing import List, Tuple

import astar as star
import processing
import processing as pr
from ptypes import RGBImagesType, CostFieldType


class Session:
    def __init__(self,
                 start: star.Node,
                 goal: star.Node,
                 rgb_images: RGBImagesType,
                 rules: List[processing.ProcessingRules]):
        self.cost_fields = pr.create_cost_fields(rgb_images, rules)
        _, y, x = self.cost_fields.shape
        self.graph = star.Graph(y, x)
        self.start = start
        self.goal = goal

    def find_path(self, weights: List[float], k: float = 1) -> Tuple[CostFieldType, List[star.Node]]:
        combined_cost_field = pr.combine_cost_fields(self.cost_fields, weights)
        self.graph.reset()
        start = time()
        path = star.a_star(start=self.start,
                           goal=self.goal,
                           h=star.get_h(k),
                           g=star.get_g(combined_cost_field),
                           graph=self.graph)
        end = time()
        print("A star completed in {}".format(end - start))
        return combined_cost_field, path
